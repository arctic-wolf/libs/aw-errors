use std::path::PathBuf;

use thiserror::Error;

/// User input errors.
#[derive(Debug, Error)]
pub enum UserError {
    #[error("invalid argument '{0}' for command '{0}'")]
    InvalidArgumentForCommand(String, String),

    #[error("invalid configuration file '{0}' provided")]
    InvalidConfigurationFile(PathBuf),
}
