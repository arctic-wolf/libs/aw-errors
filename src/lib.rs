pub mod user;

use thiserror::Error;

pub use crate::user::UserError;

#[derive(Debug, Error)]
pub enum AWError {
    #[error("an internal error occured")]
    InternalError(#[from] InternalError),

    #[error("a user input error occured")]
    UserError(#[from] UserError),
}

/// Internal errors, caused by resource/coding limits.
#[derive(Debug, Error)]
pub enum InternalError {
    #[error("no source in error")]
    NoSourceInError,

    #[error("unimplemented functionality '{0}'")]
    NotImplemented(String),

    #[error("system out of memory")]
    OutOfMemory,

    #[error("start of GUI failed with message '{0}'")]
    RunGuiFailed(String),
}
